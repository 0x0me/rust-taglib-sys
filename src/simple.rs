// rust-taglib-sys - rust ffi declartations to taglibs c bindings
// Copyright (C) 2015 Michael Engelhardt <me@mindcrime-ilab.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//! Binding of the 'simple' C interface to Rust.
extern crate libc;


/// Wraps the native file descriptior
#[repr(C)]
pub struct TagLib_File {
    dummy: libc::c_int
}

/// Wraps the native pointer to the files tag information
#[repr(C)]
pub struct TagLib_Tag {
    dummy: libc::c_int
}

/// Wraps the native pointer to the files audio properties
#[repr(C)]
pub struct TagLib_AudioProperties {
    dummy: libc::c_int
}

#[link(name="tag_c")]
extern {
    // file managment functions
    pub fn taglib_file_new(filename: *const libc::c_char) -> *mut TagLib_File;
    pub fn taglib_file_is_valid(file: *const TagLib_File) -> bool;
    pub fn taglib_file_free(file: *const TagLib_File);
    pub fn taglib_file_tag(file: *const TagLib_File) -> *mut TagLib_Tag;
    pub fn taglib_file_save(file: *const TagLib_File) -> bool;
    pub fn taglib_file_audioproperties(file: *const TagLib_File) -> *mut TagLib_AudioProperties;

    // memory management functions
    pub fn taglib_tag_free_strings();

    // tag functions - getters
    pub fn taglib_tag_album(tag: *const TagLib_Tag) -> *const libc::c_char;
    pub fn taglib_tag_artist(tag: *const TagLib_Tag) -> *const libc::c_char;
    pub fn taglib_tag_title(tag: *const TagLib_Tag) -> *const libc::c_char;
    pub fn taglib_tag_genre(tag: *const TagLib_Tag) -> *const libc::c_char;
    pub fn taglib_tag_comment(tag: *const TagLib_Tag) -> *const libc::c_char;
    pub fn taglib_tag_year(tag: *const TagLib_Tag) -> libc::c_uint;
    pub fn taglib_tag_track(tag: *const TagLib_Tag) -> libc::c_uint;

    // tag funtions - setters
    pub fn taglib_tag_set_album(tag: *const TagLib_Tag, album: *const libc::c_char);
    pub fn taglib_tag_set_artist(tag: *const TagLib_Tag, artist: *const libc::c_char);
    pub fn taglib_tag_set_title(tag: *const TagLib_Tag, title: *const libc::c_char);
    pub fn taglib_tag_set_genre(tag: *const TagLib_Tag, genre:  *const libc::c_char);
    pub fn taglib_tag_set_comment(tag: *const TagLib_Tag, comment: *const libc::c_char);
    pub fn taglib_tag_set_year(tag: *const TagLib_Tag, year: libc::c_uint);
    pub fn taglib_tag_set_track(tag: *const TagLib_Tag, track: libc::c_uint);


    // audio property functions
    pub fn taglib_audioproperties_length(audioProperties: *const TagLib_AudioProperties) -> libc::c_int;
    pub fn taglib_audioproperties_bitrate(audioProperties: *const TagLib_AudioProperties) -> libc::c_int;
    pub fn taglib_audioproperties_samplerate(audioProperties: *const TagLib_AudioProperties) -> libc::c_int;
    pub fn taglib_audioproperties_channels(audioProperties: *const TagLib_AudioProperties) -> libc::c_int;

}
