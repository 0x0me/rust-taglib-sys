// rust-taglib-sys - rust ffi declartations to taglibs c bindings
// Copyright (C) 2015 Michael Engelhardt <me@mindcrime-ilab.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//! rust-taglib-sys provides the bindings to the taglib C / C++ library. Currently only the simple
//! API is bound and can be used via the `simple` module.
//!
//! In general this library is intended to provide only a technical abstraction. From rust
//! applications or libraries refer to [rust-taglib](https://bitbucket.org/0x0me/rust-taglib).
#![crate_type = "lib"]

// in future there might be a binding of the more advanced C++ taglib interface

//! Module contains the bindings to the 'simple' C API
pub mod simple;

///////////////////////////////////////////////////////////////////////////////////////////////////
// Tests
///////////////////////////////////////////////////////////////////////////////////////////////////

#[cfg(test)]
mod test {
    use simple::{taglib_file_new,taglib_file_is_valid,taglib_file_free};
    use std::ffi::{CString};

    #[test]
    fn check_file_open() {
        let c_str_path = CString::new("resources/test_data.ogg").unwrap();
        let file = unsafe {
            let ptr = c_str_path.as_ptr();
            taglib_file_new(ptr)
        };

        let success = unsafe { taglib_file_is_valid(file) };

        println!("successful {}", success);

        unsafe {taglib_file_free(file);};

        assert!(success);
    }

}
