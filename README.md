# rust-taglib-sys[![Build Status](https://semaphoreci.com/api/v1/projects/4c47de3e-0470-4cdb-9bcb-3042a8cb6333/578250/badge.svg)](https://semaphoreci.com/0x0me/rust-taglib-sys)
## Rust FFI declarations for TagLib

This library provides the FFI declarations for the simple C bindings of the
[TagLib](https://taglib.github.io) library. TagLib allows to read and edit the
meta data of various popular audio files.

This library only provides the a wrapper around the TagLib C interface intended
being a foundation for a higher more rusty interface. See [rust-taglib](https://bitbucket.org/0x0me/rust-taglib)
for further information.

## Documentation
* [TagLib API doc](https://taglib.github.io/api/) - only functions in the simple C binding ([tag_c.h](https://github.com/taglib/taglib/blob/master/bindings/c/tag_c.h)) are supported

##License
This program is licensed under the terms of the [GNU General Public License Version 3](https://www.gnu.org/licenses/gpl-3.0),
see [https://www.gnu.org/licenses/gpl-3.0](https://www.gnu.org/licenses/gpl-3.0) for
more information.

##Development
Branch build status:
* master: [![Build Status](https://semaphoreci.com/api/v1/projects/4c47de3e-0470-4cdb-9bcb-3042a8cb6333/578250/badge.svg)](https://semaphoreci.com/0x0me/rust-taglib-sys)
* develop: [![Build Status](https://semaphoreci.com/api/v1/projects/4c47de3e-0470-4cdb-9bcb-3042a8cb6333/578252/badge.svg)](https://semaphoreci.com/0x0me/rust-taglib-sys)